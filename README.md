# Roegadyn Constructed Language

## About:

I wanted to expand the Roegadyn dictionary and add grammar rules
so that a Roegadyn-esque language could, theoretically, be spoken.

## Referenced items: 

* https://forum.square-enix.com/ffxiv/threads/61147-Roegadyn-Dictionary
* https://ffxiv.consolegameswiki.com/wiki/Languages/Roegadyn_Dictionary

## Gildrein Grammar:

Source: https://forum.square-enix.com/ffxiv/threads/61147-Roegadyn-Dictionary

### Noun to adjective

Add a +i to a noun.

Example: ais (ice) + i = aisi (icy)

Example: Aisibhir (icy ale)

(Entries which are designated as AN (both adjective & noun) will not change in structure when they are used as an adjective. For example Agat (amber) will NEVER become Agati.)

### Verb to doer noun

Add +a to a verb

Example: braen (break) +a = braena (breaker)

Example: Aisbraena (ice breaker)

### Verb to -ing noun

Add +n to a verb

Example: Floer (lead) +n = floern (leading)

Example: Floernmann (leading man)

### Letter fusing

Always remove doubled letter when combining a word that ends in the same letter the following word begins with.

Rostn + Noez = Rostnoez (Rusty Walnut)

## Naming guide

Source: https://finalfantasy.fandom.com/wiki/Roegadyn_naming_conventions

### Sea Wolves

Roegadyn names usually are formed from two words from the Roegadyn language,
such as an adjective and a noun.

Example: "Ahldskyf" is Ahld (old) and Skyf (ship)

#### Female names

The second word in a female's name is almost always:

* Swys (sister)
* Thota (daughter)
* Wyda (willow)
* Geim (jewel)
* Wyb (woman)
* Rael (doe)
* Lona (gatherer)
* Bryda (bride)

#### Male names

Male names have no typical suffix like female names

#### Surnames

Surnames include the father's forename, plus "syn" (son of) or "wyn" (daughter of).

### Hellsguard

Hellsguard names are similar to Sea Wolves names in that they follow
the adjective + noun naming pattern. However, for Hellsguard, they
use the common tongue for their names, so you get names like
"Tall Mountain" or "Blue Lily".

Hellsguard names tend to come from nature. Female names may tend to
utilize floral terms in their names.

## Sounds guide

Source: https://ffxiv.consolegameswiki.com/wiki/Languages/Roegadyn_Dictionary

### Vowels

| Letter       | Sound     | Example     | 
|--------------|-----------|-----------|
| a            |  | |
| ae           | Somewhere between the 'e' in 'egg' and the 'ai' in 'air' depending on the consonant that follows it | Aerg (ambitious) would be pronounced like 'airg', Aent (duck) would be closer to 'ent' (rhymes with 'sent') |
| e            |  | |
| i            |           | |
| o            |           | |
| oe           | OE: An 'ooh' sound such as the 'ue' in 'blue' and the 'oo' in 'I pity the foo' | Broen (brown) would be pronounced 'broon' (like 'broom'), Loef (leaf) would be 'loof' (rhymes with 'goof') 
| u            |           | |
| y            | Y: A long 'e' such as the 'ea' in 'eat' or the first 'e' in 'Steve' | Alyr (alder) would be pronounced 'ah-leer', Blyss (blossom) would be pronounced like 'bleece' (rhymes with 'fleece') 


### Consonants

| Letter       | Sound     | Example     | 
|--------------|-----------|-----------|
| pf           | Closer to an ‘f' than a ‘p'          | Pfym (five) would be 'fim' (rhymes with 'slim'), Skapf (sheep) would be 'skaff' (rhymes with 'staff')  |
| th           | More like a hard 't' than a soft 'th'          | Thosin (grey) would be 'toe-sin', Sthal (steel) would be 'stall'  |
| w            | Somewhere in-between a ‘w' and a ‘v': nowhere as hard as the ‘v' in ‘villain,' but with a little more zing than the ‘w' is ‘west'          | Wyzn (white) would sound like 'vee-zin', Wilf (wolf) would sound like 'vilf' (rhymes with 'filth')  |
| g            | Almost always hard (like the 'g' in 'guilt' but not the 'g' in 'page'), The 'gin' in Swygyn (silent) would be NOT be pronounced like the drink 'gin' but like the 'gin' in 'begin'          |  Agat (amber) would be 'ah-got'  |
| j            | A 'y' sound like in 'year' and 'yummy'          | Jungh (young) would be pronounced 'yoong'  |
| h            | When paired with a vowel (before or after), almost always silent          |  Smhid (smith) would be pronounced 'smid' (rhymes with 'kid')  |

From looking at the dictionary, "v" and "q" are not their own letters in the Roegadyn language. Instead of "v", use "w", and instead of "q", use "k".
Additionally, no words begin with "y".

## Dictionary:

View [roegadyn-dictionary.csv](oegadyn-dictionary.csv])

# Expanded grammar

Non-canon grammar by [Mhuswys "Moos" Ahldeidenwyn](https://na.finalfantasyxiv.com/lodestone/character/41244793/)

## Word Order

Since a lot of Roegadyn words are similar to common words (e.g., ais = ice, bloe = blue),
we can perhaps assume a common linguistic ancestor is shared between Roegadyn and Common.
Thus, I am assuming a Common (English) style "Subject Verb Object" ordering to sentences.

## Is, are, to be

The word "ihs" to be used in place of "is", like "A is B". Subject is a noun or an adjective.

Using "ihs" again to make the translation from Common to Roegadyn easier.

## Verb conjugation

* Present tense is using +(i)n at the end of the verb (in addition to make a verb into a noun).
* Past tense using +(e)nt (ent = end)
* Future tense using +(a)g (agynn = beginning)
* Infinitive, leave off a tense suffix, use verb as-is.

## Who, what, when, where, why?

A feature I enjoy about Esperanto is the straightforward nature of the
"who, what, when, where, why?" items. I will follow a similar form here.

Using English as a reference again for simplicity sake. You can view my first draft
[here](https://gitlab.com/rachels-conlangs/roegadyn-constructed-language/-/blob/9603930722026fda857fd49f5d587ac73e2b80f2/README.md#who-what-when-where-why), which was less
reader-friendly. :p

Statement type:
* "What/which" items will be prefixed with "w" or "wy".
* "This" (near) items will be prefixed with "th" or "thy".
* "That" (far) items will be prefixed with "tos" or "to".
* "All" items will be prefixed with "ahl".
* "None" items will be prefixed with "no" or "nor".

Topics:
* Inanimate built objects is "it"
* Inanimate natural objects is "et"
* Animate objects is "em"
* Place is based on "Laent" (land)
* Time is based on "Moen" (moon)
* Amount is based on "Lyng" (long)
* Reason is "kuz" (like "because" shortened)

| -                           | Which?     | This       | That       | All        | None       |
|-----------------------------|------------|------------|------------|------------|------------|
| Object (inanimate, built)   | wit        | thit       | tosit      | ahlit      | norit      |
| Object (inanimate, natural) | wet        | thet       | toset      | ahlet      | noret      |
| Object (animate)            | wem        | tem        | tosem      | ahlem      | norem      |
| Place                       | wylaent    | thylaent   | tolaent    | ahlaent    | nolaent    |
| Time                        | wymoen     | thymoen    | tomoen     | ahlmoen    | nomoen     |
| Amount                      | wylyng     | thylyng    | tolyng     | ahlyng     | nolyng     |
| Reason                      | wykuz      | thykuz     | tokuz      | ahlkuz     | nokuz      |


## Pronouns

The Roegadyn dictionary does not specify any pronouns, which can make communication difficult.

|                | Singular | Plural |
|----------------|----------|--------|
| I, me, us, we  | maen     | maene  |
| You            | tum      | tume   |
| He, She, They  | Use animate object form   | Suffix with -sk |
| It             | Use inanimate object form | Suffix with -sk |

## Prepositions

Trying to keep close to Common, for simplicity.

| Common           | Roegadyn | Example             |
|------------------|----------|---------------------|
| At (time)        | et       |                     |
| At (location)    | er       |                     |
| In               | en       |                     |
| On               | aen      |                     |
| Of               | apf      |                     |
| From             | froem    |                     |
| To               | tyu      |                     |
| For              | foer     |                     |
| With             | wif      |                     |

## Modifiers
| Common   | Roegadyn | Example                          |
|----------|----------|----------------------------------|
| To begin | -agynn   | Wifagynn = With-begin = to join, begin participating with |
| To end   | -ent     | Wifent = With-end = to leave     |

## Questions

To turn a declarative statement (e.g., "I will fight" / "Maen fhetag") to a question
("Will I fight?"), add a "kya" at the beginning.

## Miscellaneous

No articles like "a" or "the". You can use a number like "one" in place of "a" as needed.

## Example sentences

| Common            | Roegadyn      | Notes                                             |
|-------------------|---------------|---------------------------------------------------|
| Greetings!        | Guhtagynn     | Literally, "Good beginning" (official dictionary) |
| Good game         | Ghutfhet      | Literally, "Good fight" (official dictionary)     |
| Bad luck          | Grymsald      | Literally, "Cruel luck" (official dictionary)     |
| I am fighting     | Maen fhetin     | I fight-present-tense                             |
| I fought          | Maen fhetent    | I fight-past-tense                                |
| I will fight      | Maen fhetag     | I fight-future-tense                              |
| I am fighting you | Maen fhetin tum | I fight-present-tense you                         |
| I am Mhuswys      | Maen ihs Mhuswys | I am (name)                                       |
| I am sad          | Maen ihs caer    | I am sad                                          |
| It is yellow      | Toset ihs fhil | It (natural) is yellow                            |
| It is yellow      | Tosit ihs fhil | It (built thing) is yellow                        |
| Is it yellow?     | Kya tosit ihs fhil | (Question) it is (built thing) yellow?                        |

Note that in English we use the "am" verb when working with our verbs, which is a really unusual thing for languages. When using Roegadyn, and most other languages for that matter, forget your tendency to want to use "am" with verbs in that way.

## Dialogues

### Duty Roulettes

| Common            | Roegadyn      | Notes                                             |
|-------------------|---------------|---------------------------------------------------|
| I will join a duty roulette. | ** Maen wifagynnag Doeti Roelhet. ** | I with-begin-futuretense duty roulette. |
| Do you want to join a duty roulette? | **Kya tum dhesirin wifagynn Doeti Roelhet?** | Question you want-presenttense join duty roulette? The second verb (join) is infinitive and can remain without a suffix |
| Yes, I will join with you. | **Han, maen wifagynnag wif tum** | |

### Where are you?

| Common            | Roegadyn             | Notes                                             |
|-------------------|----------------------|---------------------------------------------------|
| Where are you?    | **Kya wylaent tum?** | Question where you? |
| I am in Limsa Lominsa | **Maen ihs en Limsa Lominsa** | |
| I will go to you! | **Maen pfarag tyu tum!** | "pfar" means "walking", could be used as "to go/to come" with the direction being specified by the preposition. |
| I came from Ul'Dah. | **Maen pfarent froem Ul'Dah.**








